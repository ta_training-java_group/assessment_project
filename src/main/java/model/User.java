package model;

public class User {
    private String userEmail;
    private String password;
    public User(String username, String password){
        this.userEmail = username;
        this.password = password;
    }

    public String getUserEmail() {

        return userEmail;
    }

    public void setUserEmail(String username) {
        this.userEmail = username;

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
