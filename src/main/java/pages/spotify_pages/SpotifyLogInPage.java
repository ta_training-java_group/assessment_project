package pages.spotify_pages;

import model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class SpotifyLogInPage extends BasePage {
    @FindBy(xpath = "//input[@placeholder='Email or username']")
    private WebElement spotifyEmailUserField;

    @FindBy(xpath = "//input[@placeholder='Password']")
    private WebElement spotifyPasswordField;

    @FindBy(xpath = "//span[text()='Log In']")
    private WebElement logInButton;

    @FindBy(xpath = "//span[text()='Please enter your password.']")
    private WebElement emptyPasswordPopup;

    @FindBy(xpath = "//p[text()='Please enter your Spotify username or email address.']")
    private WebElement emptyUserPopUp;

    @FindBy(xpath = "//span[text()='Incorrect username or password.']")
    private WebElement incorrectUserOrPasswordPopUp;

    @FindBy(xpath = "//button[@aria-label='Andrei']")
    private WebElement userName;

    public SpotifyLogInPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public SpotifyLogInPage spotifyUserCredentialsInput(User user){
        waitElementIsVisible(spotifyEmailUserField, 15).sendKeys(user.getUserEmail());
        spotifyPasswordField.sendKeys(user.getPassword());
        return this;
    }

    public SpotifyLogInPage spotifyClearUserLogInCredentials(){
        spotifyEmailUserField.clear();
        spotifyPasswordField.clear();
        spotifyEmailUserField.sendKeys(" \b");
        spotifyPasswordField.sendKeys(" \b");
        return this;
    }

    public SpotifyLogInPage spotifyUserEmailInput(User user) {
        waitElementIsVisible(spotifyEmailUserField, 15).sendKeys(user.getUserEmail());
        return this;
    }

    public SpotifyLogInPage spotifyPasswordInput(User user) {
        spotifyPasswordField.sendKeys(user.getPassword());
        return this;
    }

    public SpotifyLoggedInUsersPage spotifyLogInButtonClickSuccess() {
        logInButton.click();
        waitElementIsVisible(userName, 20);
        return new SpotifyLoggedInUsersPage(driver);
    }

    public SpotifyLogInPage spotifyLogInButtonClickFail() {
        logInButton.click();
        return this;
    }

    public boolean getEmptyPasswordPopUp() {
        return waitElementIsVisible(emptyPasswordPopup, 15).isDisplayed();
    }
    public boolean getEmptyUserPopUp() {
        return waitElementIsVisible(emptyUserPopUp, 15).isDisplayed();
    }

    public boolean incorrectUserOrPasswordPopUpIsVisible() {
        return waitElementIsVisible(incorrectUserOrPasswordPopUp, 15).isDisplayed();
    }

}
