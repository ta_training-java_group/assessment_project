package pages.spotify_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class SpotifyLoggedInUsersPage extends BasePage {
    @FindBy(xpath = "//button[@aria-label='Andrei']")
    private WebElement userName;
    public SpotifyLoggedInUsersPage(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }
    public boolean usernameIsVisible() {
        waitElementIsVisible(userName, 5);
        return true;
    }
}
