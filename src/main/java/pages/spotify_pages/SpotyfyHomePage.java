package pages.spotify_pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.base_page.BasePage;

public class SpotyfyHomePage extends BasePage {
    @FindBy(xpath = "//span[text()='Log in']")
    private WebElement spotifyLogInButton;

    public SpotyfyHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public SpotifyLogInPage spotifyLogIn(){
        waitElementIsVisible(spotifyLogInButton,15).click();
        return new SpotifyLogInPage(driver);
    }
}
