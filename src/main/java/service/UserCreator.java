package service;

import model.User;

public class UserCreator {

    private static final String spotifyUserEmail = TestDataReader.getTestData("test.spotifyUser.email");
    private static final String spotifyUserPassword = TestDataReader.getTestData("test.spotifyUser.password");
    private static final String invalidPassword = "qwerty";


    public static User spotifyUserWithCredentialsFromProperty() {
        return new User(spotifyUserEmail, spotifyUserPassword);
    }
    public static User spotifyUserWithWrongPassword() {
        return new User(spotifyUserEmail, invalidPassword);
    }

}
