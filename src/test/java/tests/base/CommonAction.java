package tests.base;

import driver.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import pages.spotify_pages.SpotifyLogInPage;
import pages.spotify_pages.SpotifyLoggedInUsersPage;
import pages.spotify_pages.SpotyfyHomePage;
import utils.TestListener;

@Listeners({TestListener.class})
public class CommonAction {
    protected WebDriver driver;
    protected SpotyfyHomePage spotyfyHomePage;
    protected SpotifyLogInPage spotifyLogInPage;
    protected SpotifyLoggedInUsersPage spotifyLoggedInUsersPage;
    protected final String spotifyHomeUrl = "https://open.spotify.com//";

    @BeforeMethod()
    public void setUp(){
        driver = DriverSingleton.createDriver();
        spotyfyHomePage = new SpotyfyHomePage(driver);
        spotifyLogInPage = new SpotifyLogInPage(driver);
        spotifyLoggedInUsersPage = new SpotifyLoggedInUsersPage(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void finishBrowser() {
        DriverSingleton.closeDriver();
        driver = null;
    }
}
