package tests.smoke;

import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import service.UserCreator;
import tests.base.CommonAction;

public class Smoke extends CommonAction {

    @Test
    public void validLoginPassword() {
        User spotifyUser = UserCreator.spotifyUserWithCredentialsFromProperty();
        spotyfyHomePage
                .open(spotifyHomeUrl);
        spotyfyHomePage
                .spotifyLogIn();
        spotifyLogInPage
                .spotifyUserEmailInput(spotifyUser)
                .spotifyPasswordInput(spotifyUser)
                .spotifyLogInButtonClickSuccess();
        Assert.assertTrue(spotifyLoggedInUsersPage.usernameIsVisible());
    }

    @Test
    public void emptyPasswordAndUserName() {
        User spotifyUser = UserCreator.spotifyUserWithCredentialsFromProperty();
        spotyfyHomePage
                .open(spotifyHomeUrl);
        spotyfyHomePage
                .spotifyLogIn();
        spotifyLogInPage
                .spotifyUserCredentialsInput(spotifyUser)
                .spotifyClearUserLogInCredentials();
        Assert.assertTrue(spotifyLogInPage.getEmptyPasswordPopUp() && spotifyLogInPage.getEmptyUserPopUp());
    }

    @Test
    public void invalidPassword() {
        User spotifyUserWithInvalidCredentials = UserCreator.spotifyUserWithWrongPassword();
        spotyfyHomePage
                .open(spotifyHomeUrl);
        spotyfyHomePage
                .spotifyLogIn();
        spotifyLogInPage
                .spotifyUserEmailInput(spotifyUserWithInvalidCredentials)
                .spotifyPasswordInput(spotifyUserWithInvalidCredentials)
                .spotifyLogInButtonClickFail();
        Assert.assertTrue(spotifyLogInPage.incorrectUserOrPasswordPopUpIsVisible());
    }
}
